(function () {
  if (!('mediaSession' in navigator)) {
      return
  }

  let api = externalAPI
  let ms = navigator.mediaSession

  ms.setActionHandler('play', () => api.togglePause(false))
  ms.setActionHandler('pause', () => api.togglePause(true))
  ms.setActionHandler('previoustrack', () => api.prev())
  ms.setActionHandler('nexttrack', () => api.next())

  function updateMetadata() {
    let track = api.getCurrentTrack()

    ms.metadata = null
    if (!track) {
      return
    }

    ms.metadata = new MediaMetadata({
      title: track.title,
      artist: track.artists.map((a) => a.title).join(', '),
      album: track.album.title,
      artwork: [{src: 'https://' + track.cover.replace('%%', '400x400')}]
    })
  }

  function updateProgress() {
    ms.setPositionState({
      duration: api.getProgress().duration * 1000000,
      playbackRate: 1,
      position: api.getProgress().position * 1000000
    })
  }

  function updateControls() {
    ms.playbackState = api.isPlaying() ? "playing" : "paused"
  }

  api.on(api.EVENT_TRACK, updateMetadata)
  api.on(api.EVENT_PROGRESS, updateProgress)
  api.on(api.EVENT_STATE, updateControls)
  api.on(api.EVENT_CONTROLS, updateControls)
})()
