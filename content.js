(function() {
  var browser = browser || chrome
  const body = document.querySelector('body')
  const pageScript = document.createElement('script')
  pageScript.setAttribute('type', 'text/javascript')
  pageScript.setAttribute('src', browser.runtime.getURL('page.js'))
  body.append(pageScript)
})()
