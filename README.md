# YaMu

YaMu is a browser extension that integrates Yandex.Music with the operating system, using your browser's MediaSession API.

## Building and installing

This requires the `web-ext` tool, which can be obtained from your distribution's repositories or through `npm install --global web-ext`, `yarn global add web-ext` or similar.

Run `web-ext build`, the extension will be produced in the `web-ext-artifacts` subdirectory.

You can also run `web-ext sign` to sign the extension, allowing it to run on stable Firefox.

To use the extension on Chrome, you currently need to manually upload it to Google's store, which requires a one time payment. We primarily care about Firefox here, so you're (mostly) on your own. Pull requests for better Chrome support will still be welcomed, unless they make things significantly more complicated.
